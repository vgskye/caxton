## Environment

* **Caxton version:** (The version of Caxton you used. If you built the mod from source, then also provide the hash of the commit you built it from (`git rev-parse --short HEAD`).)
* **Minecraft version:** ...
* **Mod loader and version:** (e.g. ‘Forge 47.1.3’ or ‘Fabric Loader 0.14.21’. If using Fabric or Quilt, also specify the Fabric API or Quilted Fabric API version; e.g. ‘Fabric API 0.87.0+1.20.1’.)
* **Operating system and CPU architecture:** ...

## Summary

(Provide a concise summary of the bug you encountered)

## Steps to reproduce

(How to reproduce the issue - this is very important)

**Expected behavior:** (What you expect to see)

**Actual behavior:** (What happens instead)

## Relevant logs and/or screenshots

Attach your Minecraft log file (use the paperclip icon in the editor) or upload it to a site such as [mclo.gs](https://mclo.gs/).
This can be found as `logs/latest.log` in the instance folder.

Also put any screenshots describing the issue here if necessary.
