use std::slice;

use rustybuzz::{Face, GlyphBuffer, GlyphInfo, GlyphPosition};
use ttf_parser::{colr::Painter, GlyphId, RgbaColor};

#[repr(transparent)]
pub struct ShapingResultEntry(pub [i32; 6]);

pub struct ShapingResult {
    pub data: Vec<ShapingResultEntry>,
    pub total_width: i32,
}

struct ShapingResultEntryPainter<'a> {
    ok: bool,
    entries: &'a mut Vec<ShapingResultEntry>,
    position: &'a GlyphPosition,
    info: &'a GlyphInfo,
    pending: Option<GlyphId>,
    count: usize,
}

impl<'a> Painter<'a> for ShapingResultEntryPainter<'a> {
    fn outline_glyph(&mut self, glyph_id: GlyphId) {
        self.pending = Some(glyph_id)
    }

    fn paint(&mut self, paint: ttf_parser::colr::Paint<'a>) {
        let color = if let ttf_parser::colr::Paint::Solid(color) = paint { color } else {
            self.ok = false;
            return;
        };
        self.entries.push(ShapingResultEntry([
            (self.pending.expect("paint_color called without outline").0 as i32)
                | (self.info.unsafe_to_break() as i32) << 24
                | 0b11 << 25,
            self.info.cluster as i32,
            0,
            ((color.red as u32) << 24
                | (color.green as u32) << 16
                | (color.blue as u32) << 8
                | color.alpha as u32) as i32,
            self.position.x_offset,
            self.position.y_offset,
        ]));
        self.count += 1;
    }

    fn push_clip(&mut self) {
        self.ok = false;
    }

    fn push_clip_box(&mut self, clipbox: ttf_parser::colr::ClipBox) {
        self.ok = false;
    }

    fn pop_clip(&mut self) {
        self.ok = false;
    }

    fn push_layer(&mut self, mode: ttf_parser::colr::CompositeMode) {
        self.ok = false;
    }

    fn pop_layer(&mut self) {
        self.ok = false;
    }

    fn push_translate(&mut self, tx: f32, ty: f32) {
        self.ok = false;
    }

    fn push_scale(&mut self, sx: f32, sy: f32) {
        self.ok = false;
    }

    fn push_rotate(&mut self, angle: f32) {
        self.ok = false;
    }

    fn push_skew(&mut self, skew_x: f32, skew_y: f32) {
        self.ok = false;
    }

    fn push_transform(&mut self, transform: ttf_parser::Transform) {
        self.ok = false;
    }

    fn pop_transform(&mut self) {
        self.ok = false;
    }
}

impl<'a> ShapingResultEntryPainter<'a> {
    fn finalize(&mut self) {
        self.entries.last_mut().expect("No layers?").0[0] &= !(1 << 26); // clear isGlyphRun flag
        self.entries.last_mut().expect("No layers?").0[2] = self.position.x_advance;
    }
    fn reset(&mut self) {
        self.entries.truncate(self.entries.len() - self.count);
    }
}

impl ShapingResult {
    pub fn from_glyph_buffer(buffer: &GlyphBuffer, face: &Face) -> Self {
        let mut data = Vec::with_capacity(buffer.len());
        let mut total_width = 0;

        let glyph_infos = buffer.glyph_infos();
        let glyph_positions = buffer.glyph_positions();

        let has_colr = face.tables().colr.is_some();

        for i in 0..buffer.len() {
            let info = &glyph_infos[i];
            let position = &glyph_positions[i];
            
            total_width += position.x_advance;

            if has_colr && face.is_color_glyph(GlyphId(info.glyph_id as u16)) {
                let mut painter = ShapingResultEntryPainter {
                    ok: true,
                    entries: &mut data,
                    position,
                    info,
                    pending: None,
                    count: 0,
                };
    
                if face
                    .paint_color_glyph(GlyphId(info.glyph_id as u16), 0, RgbaColor::new(255, 255, 255, 0), &mut painter)
                    .is_some()
                    && painter.ok
                {
                    painter.finalize();
                    continue;
                } else {
                    painter.reset();
                }
            }

            data.push(ShapingResultEntry([
                (info.glyph_id as i32) | (info.unsafe_to_break() as i32) << 24,
                info.cluster as i32,
                position.x_advance,
                0,
                position.x_offset,
                position.y_offset,
            ]));
        }

        ShapingResult { data, total_width }
    }

    pub fn data_as_i32s(&self) -> &[i32] {
        let slice = &self.data[..];
        // SAFETY: slice has the same layout as [[i32; 6]], which we can transmute to [i32]
        unsafe { slice::from_raw_parts(slice.as_ptr().cast::<i32>(), 6 * slice.len()) }
    }
}
