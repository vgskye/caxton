// (C) 2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.command;

public class DebugShapeInfo {
  public String buffer;
  public String inferredLanguage, inferredScript, inferredDirection;
}
