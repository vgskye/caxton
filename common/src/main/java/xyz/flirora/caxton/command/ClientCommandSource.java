// (C) 2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.command;

import java.util.function.Supplier;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.Text;

/** A custom command source for client commands that abstracts between platform differences. */
public interface ClientCommandSource {
  void sendFeedback(Supplier<Text> message);

  void sendError(Text message);

  MinecraftClient getClient();
}
