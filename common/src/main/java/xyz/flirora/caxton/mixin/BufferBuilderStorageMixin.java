// (C) 2022-2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.mixin;

import java.util.SequencedMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.BufferBuilderStorage;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.BufferAllocator;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import xyz.flirora.caxton.CaxtonModClient;
import xyz.flirora.caxton.render.WorldRendererVertexConsumerProvider;

@Environment(EnvType.CLIENT)
@Mixin(BufferBuilderStorage.class)
public class BufferBuilderStorageMixin {
  @Redirect(
      method = "<init>",
      at =
          @At(
              value = "INVOKE",
              target =
                  "Lnet/minecraft/client/render/VertexConsumerProvider;immediate(Ljava/util/SequencedMap;Lnet/minecraft/client/util/BufferAllocator;)Lnet/minecraft/client/render/VertexConsumerProvider$Immediate;"),
      slice =
          @Slice(
              from =
                  @At(
                      value = "FIELD",
                      opcode = Opcodes.PUTFIELD,
                      target =
                          "Lnet/minecraft/client/render/BufferBuilderStorage;effectVertexConsumers:Lnet/minecraft/client/render/VertexConsumerProvider$Immediate;"),
              to =
                  @At(
                      value = "FIELD",
                      opcode = Opcodes.PUTFIELD,
                      target =
                          "Lnet/minecraft/client/render/BufferBuilderStorage;entityVertexConsumers:Lnet/minecraft/client/render/VertexConsumerProvider$Immediate;")),
      allow = 1)
  private VertexConsumerProvider.Immediate redirect(
      SequencedMap<RenderLayer, BufferAllocator> layerBuffers, BufferAllocator fallbackBuffer) {
    if (CaxtonModClient.FORCE_DISABLE_BATCHING_HACK)
      return VertexConsumerProvider.immediate(layerBuffers, fallbackBuffer);
    return new WorldRendererVertexConsumerProvider(fallbackBuffer, layerBuffers);
  }
}
