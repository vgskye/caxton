// (C) 2022-2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.layout.gui;

public interface TextFieldWidgetExt {
  void updateCaxtonText(boolean changed);
}
